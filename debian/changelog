piexif (1.1.3-3) UNRELEASED; urgency=medium

  * Update standards version to 4.6.2, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Tue, 10 Jan 2023 07:35:54 -0000

piexif (1.1.3-2) unstable; urgency=medium

  * Rebuild with no source changes.

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 27 Nov 2021 14:40:55 +0100

piexif (1.1.3-1) unstable; urgency=medium

  [ Andrej Shadura ]
  * Reintroduce the package to Debian.
  * New upstream release.
  * Switch to DEP-14.
  * Update the watch file.
  * Update the upstream metadata.
  * Update the copyright file.
  * Refresh the packaging.
  * Add patches for upstream issues.
  * Use pytest to run tests.
  * Clean egg-info.
  * Use secure URI in debian/watch.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 19 Sep 2021 13:40:30 +0100

piexif (1.0.13-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #937287

 -- Sandro Tosi <morph@debian.org>  Thu, 12 Dec 2019 19:46:32 -0500

piexif (1.0.13-2) unstable; urgency=medium

  [ Marcelo Jorge Vieira ]
  * d/control: Fixed maintainer, added Debian Python Modules Team
  * d/copyright: Added OFL-1.1

  [ Ondřej Nový ]
  * d/watch: Use https protocol

 -- Marcelo Jorge Vieira <metal@debian.org>  Wed, 05 Dec 2018 22:59:02 -0200

piexif (1.0.13-1) unstable; urgency=low

  * Initial release (closes: #844441)

 -- Marcelo Jorge Vieira <metal@debian.org>  Sat, 16 Jun 2018 12:25:34 -0300
